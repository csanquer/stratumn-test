# set default shell
SHELL := $(shell which bash)

# default shell options
.SHELLFLAGS = -c

.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell

default: all;   # default target

all:
	$(MAKE) fixtures
	$(MAKE) test
	$(MAKE) clean
.PHONY: all

fixtures: 
	./setup_fixtures.sh
.PHONY: fixtures

clean: 
	./teardown.sh
.PHONY: clean

test: 
	./test.sh
.PHONY: test