stratumn-test
=============

Requirements
------------

* GNU make
* [aws cli](https://aws.amazon.com/fr/cli/)
* [jq](https://stedolan.github.io/jq/)

Usage
-----

### prepare credentials

copy `env.dist.sh` to `env.sh` and edit it with AWS API credentials

### create fixtures

```sh
make fixtures
```

### test 

delete Network Interfaces with status available and security groups starting with "lamdba-"

```sh
make test
```

### Clean all fixtures

```sh
make clean
```

### all steps

```sh
make all
```
