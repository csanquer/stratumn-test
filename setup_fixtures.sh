#!/bin/bash

if [ ! -f "env.sh" ]; then
    echo "credentials not set"
    exit 1 
fi 

source env.sh

region=eu-west-3
az=${region}a

security_groups=("test-1" "lambda-test-1" "lambda-test-2")

common_options="--region ${region} --output json"

get_subnet_id() {
    echo $(aws --profile stratumn ec2 describe-subnets --filter Name=availability-zone,Values=${az} | jq -r ".Subnets[0].SubnetId")
}

create_sg() {
    local name=$1
    local sg_id
    if [ $(aws ec2 describe-security-groups $common_options --filters "Name=group-name,Values=$name" | jq -r ".SecurityGroups| length") -ge 1 ]; then
        # Security Group already created"
        sg_id=$(aws ec2 describe-security-groups $common_options --filters "Name=group-name,Values=$name" | jq -r ".SecurityGroups[0].GroupId")
    else
        # Create Security Group
        sg_id=$(aws ec2 create-security-group $common_options --group-name "$name" --description "$name" | jq -r ".GroupId")
    fi

    echo $sg_id
}

create_eni() {
    local name=$1
    local sg_id=$2
    local subnet_id=$3
    local eni_id

    if [ $(aws ec2 describe-network-interfaces $common_options --filters "Name=group-id,Values=$sg_id" | jq -r ".NetworkInterfaces| length") -ge 1 ]; then
        eni_id=$(aws ec2 describe-network-interfaces $common_options --filters "Name=group-id,Values=$sg_id" | jq -r ".NetworkInterfaces[0].NetworkInterfaceId")
    else
        eni_id=$(aws ec2 create-network-interface $common_options --subnet-id $subnet_id --groups $sg_id --description "$name ENI" | jq -r ".NetworkInterface.NetworkInterfaceId")
    fi

    echo $eni_id
}


subnet_id=$(get_subnet_id)

for sg_name in ${security_groups[*]}; do
    sg_id=$(create_sg $sg_name)
    echo "Security group created $sg_id"

    eni_id=$(create_eni $sg_name $sg_id $subnet_id)
    echo "Network Interface created $eni_id"
done

