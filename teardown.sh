#!/bin/bash

if [ ! -f "env.sh" ]; then
    echo "credentials not set"
    exit 1 
fi 

source env.sh

region=eu-west-3
az=${region}a

security_groups=("test-1" "lambda-test-1" "lambda-test-2")

common_options="--region ${region} --output json"

get_sg_id() {
    echo $(aws ec2 describe-security-groups $common_options --filters "Name=group-name,Values=$1" | jq -r ".SecurityGroups[0].GroupId")
}

get_eni_ids() {
    echo $(aws ec2 describe-network-interfaces $common_options --filters "Name=group-id,Values=$1" | jq -r ".NetworkInterfaces[].NetworkInterfaceId")
}

delete_sg() {
    aws ec2 delete-security-group $common_options --group-id $1
    # return $?
}

delete_eni() {
    aws ec2 delete-network-interface $common_options --network-interface-id $1
}

for sg_name in ${security_groups[*]}; do
    sg_id=$(get_sg_id $sg_name)

    for eni_id in $(get_eni_ids $sg_id); do
        # echo $eni_id
        delete_eni $eni_id
        if [ $? -eq 0 ]; then
            echo Network Interface $eni_id deleted
        fi
    done

    delete_sg $sg_id
    if [ $? -eq 0 ]; then
        echo Security group $sg_id deleted
    fi
done
