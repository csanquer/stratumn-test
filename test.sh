#!/bin/bash

if [ ! -f "env.sh" ]; then
    echo "credentials not set"
    exit 1 
fi 

source env.sh

region=eu-west-3
az=${region}a

sg_pattern="lambda-*"

common_options="--region ${region} --output json"

delete_eni() {
    aws ec2 delete-network-interface $common_options --network-interface-id $1
}

count_available_eni() {
    echo $(aws ec2 describe-network-interfaces $common_options --filters "Name=status,Values=available" | jq -r ".NetworkInterfaces | length")
}

search_eni() {
    aws ec2 describe-network-interfaces $common_options --filters "Name=group-name,Values=$1" "Name=status,Values=available" | jq -r ".NetworkInterfaces[].NetworkInterfaceId"
}

echo available ENIs = $(count_available_eni)
echo Searching Alvailable Network Interface with Security Group $sg_pattern
eni_ids=($(search_eni $sg_pattern))

if [ $? -eq 0 ]; then
    echo ${#eni_ids[*]} Network Interfaces found

    for eni_id in ${eni_ids[*]}; do
        delete_eni $eni_id

        if [ $? -eq 0 ]; then
            echo "Network Interface $eni_id deleted"
        fi
    done
fi

echo available ENIs = $(count_available_eni)



